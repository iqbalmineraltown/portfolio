---
preview-title: Gasing Mania ZX
preview-banner: /assets/img/portfolio/gasingmaniazx.jpg
title: Gasing Mania ZX
banner: https://www.youtube.com/watch?v=s1BpxxI7Kdk
banner-type: youtube
details:
 - label: Role
   value: Programmer, Project Manager 
 - label: Tech Stack
   value: Construct 2 (Javascript)
 - label: Platform
   value: Android
 - label: Team Size
   value: 3 Person
 - label: Duration
   value: 2 Months
notes: This was the the final assignment of Mobile Technology course on my undergraduate program. We were supposed to create a mobile app which utilized its sensors. So we thought, "why not make a game?". We utilized gyroscope and accelerometer for the main gameplay and it turned out great! Sadly, this app was only maintained for a year and has been taken down from the store ever since.
---