---
preview-title: Almightree (mobile)
preview-banner: /assets/img/portfolio/almightree.jpg
title: Almightree The Last Dreamer (mobile)
banner: https://youtu.be/KghH01OAd1g
banner-type: youtube
details:
 - label: Role
   value: Programmer, Analyst
 - label: Tech Stack
   value: Unity3D (C#), Android SDK, Xcode
 - label: Platform
   value: iOS, Android
 - label: Team Size
   value: 5 Person
 - label: Duration
   value: 5 Months
notes: This game already released for a year on apple appstore when I joined. My responsibilities are porting iOS version for  Android (Google and Amazon) store, improve controls, data analytics, multi-resolution adaptability, auto-set graphics settings based on device's hardware specs, add localization to popular languages, and implement achievements with each respective store account. I also responsible for free version of this game with in-app purchase and ads, published to China-region appstore.
link: exist
itunes: https://itunes.apple.com/id/app/almightree-the-last-dreamer/id895436237
googleplay: https://play.google.com/store/apps/details?id=com.crescentmoongames.almightree
amazon: https://www.amazon.com/Crescent-Moon-Games-Almightree-Dreamer/dp/B00ZFY94HG
---