---
preview-title: Almightree (desktop)
preview-banner: /assets/img/portfolio/almightreepc.jpg
title: Almightree The Last Dreamer (desktop)
banner: https://steamcdn-a.akamaihd.net/steam/apps/256656028/movie480.webm
banner-type: video
details:
 - label: Role
   value: Programmer, Analyst
 - label: Tech Stack
   value: Unity3D (C#), SteamSDK
 - label: Platform
   value: Windows, Mac OSX, SteamOS(Linux)
 - label: Team Size
   value: 5 Person
 - label: Duration
   value: 5 Months
notes: This was after the mobile version. We ported mobile version to desktop (distributed via Steam) and supports major operating systems. In addition to reimplementation of mobile version improvements, I was responsible for porting to desktop, controller-support implementation, enable users to set graphic settings, and adding local multiplayer mode.
link: exist
steam: https://store.steampowered.com/app/368050/Almightree_The_Last_Dreamer/
---