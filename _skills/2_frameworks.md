---
label: Frameworks, as my solid armor
list:
 - image: /assets/img/skills/flutter.svg
   name: Flutter
 - image: /assets/img/skills/mvvmcross.png
   name: Mvvmcross (Xamarin)
 - image: /assets/img/skills/vuejs.svg
   name: Vue.js
 - image: /assets/img/skills/react.svg
   name: ReactJS
---