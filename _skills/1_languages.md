---
label: Languages, as my sharpest weapons
list:
 - image: /assets/img/skills/dart.svg
   name: Dart
 - image: /assets/img/skills/csharp.png
   name: C#
 - image: /assets/img/skills/java.png
   name: Java
 - image: /assets/img/skills/bash.svg
   name: Bash
 - image: /assets/img/skills/powershell.png
   name: PowerShell
 - image: /assets/img/skills/html5.png
   name: HTML5 + CSS3
 - image: /assets/img/skills/javascript.png
   name: Javascript
---