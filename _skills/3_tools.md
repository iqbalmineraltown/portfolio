---
label: Tools, as my helpful companions
list:
 - image: /assets/img/skills/phabricator.png
   name: Phabricator
 - image: /assets/img/skills/jenkins.svg
   name: Jenkins CI
 - image: /assets/img/skills/git.svg
   name: Git
 - image: /assets/img/skills/vscode.svg
   name: Visual Studio Code
 - image: /assets/img/skills/visualstudio.svg
   name: Visual Studio
 - image: /assets/img/skills/androidstudio.svg
   name: Android Studio (IntelliJ)
---